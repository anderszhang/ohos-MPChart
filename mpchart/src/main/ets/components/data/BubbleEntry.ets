/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Utils from '../utils/Utils';//此行代码勿删
import { ImagePaint } from './Paint';
import EntryOhos from './EntryOhos';

/**
 * Subclass of Entry that holds a value for one entry in a BubbleChart. Bubble
 *
 */
export default class BubbleEntry extends EntryOhos {
    /** size value */
    private mSize:number = 0;

    constructor(x:number, y?:number, size?:number, icon?:ImagePaint, data?:Object){
      super(x,y,icon,data);
      this.mSize = size;
    }

    public copy():BubbleEntry {
        var c:BubbleEntry = new BubbleEntry(this.getX(), this.getY(), this.mSize,this.getIcon(), this.getData());
        return c;
    }

    /**
     * Returns the size of this entry (the size of the bubble).
     *
     * @return
     */
    public getSize():number {
        return this.mSize;
    }

    public setSize( size:number):void {
        this.mSize = size;
    }
}
