/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type IShapeRenderer from './IShapeRenderer';
import type IScatterDataSet from '../../interfaces/datasets/IScatterDataSet';
import ViewPortHandler from '../../utils/ViewPortHandler';
import Paint,{Style, CirclePaint} from '../../data/Paint'
import Utils from '../../utils/Utils';
import ColorTemplate from '../../utils/ColorTemplate';

/**
 * Created by wajdic on 15/06/2016.
 * Created at Time 09:08
 */
export default class CircleShapeRenderer implements IShapeRenderer
{

    public renderShape(paints: Paint[], dataSet: IScatterDataSet, viewPortHandler: ViewPortHandler,
                            posX: number, posY: number, renderPaint: Paint): void {

        let shapeSize: number = dataSet.getScatterShapeSize();
        let shapeHalf: number = shapeSize / 2;
        let shapeHoleSizeHalf: number = Utils.convertDpToPixel(dataSet.getScatterShapeHoleRadius());
        let shapeHoleSize: number = shapeHoleSizeHalf * 2;
        let shapeStrokeSize: number = (shapeSize - shapeHoleSize) / 2;
        let shapeStrokeSizeHalf: number = shapeStrokeSize / 2;

        let shapeHoleColor: number = dataSet.getScatterShapeHoleColor();

        if (shapeSize > 0.0) {
            renderPaint.setStyle(Style.STROKE);
            renderPaint.setStrokeWidth(shapeStrokeSize);

            let circlePaint : CirclePaint = new CirclePaint();
            let radius = shapeHoleSizeHalf + shapeStrokeSizeHalf;
            circlePaint.set(renderPaint);
            circlePaint.setStyle(Style.FILL)
            circlePaint.setX(posX-radius)
            circlePaint.setY(posY-radius)
            circlePaint.setStrokeRadius(radius);
            circlePaint.setWidth(radius*2)
            circlePaint.setHeight(radius*2)
            paints.push(circlePaint);

            if (shapeHoleColor != ColorTemplate.COLOR_NONE) {
                renderPaint.setStyle(Style.FILL);

                renderPaint.setColor(shapeHoleColor);

                let circlePaint : CirclePaint = new CirclePaint();
                circlePaint.set(renderPaint);
                circlePaint.setStyle(Style.FILL)
                circlePaint.setX(posX-shapeHoleSizeHalf)
                circlePaint.setY(posY-shapeHoleSizeHalf)
                circlePaint.setStrokeRadius(shapeHoleSizeHalf);
                circlePaint.setWidth(shapeHoleSizeHalf*2)
                circlePaint.setHeight(shapeHoleSizeHalf*2)
                paints.push(circlePaint);
            }
        } else {
            renderPaint.setStyle(Style.FILL);

            let circlePaint : CirclePaint = new CirclePaint();
            circlePaint.set(renderPaint);
            circlePaint.setStyle(Style.FILL)
            circlePaint.setX(posX-shapeHalf)
            circlePaint.setY(posY-shapeHalf)
            circlePaint.setStrokeRadius(shapeHalf);
            circlePaint.setWidth(shapeHalf*2)
            circlePaint.setHeight(shapeHalf*2)
            paints.push(circlePaint);
        }

    }

}
