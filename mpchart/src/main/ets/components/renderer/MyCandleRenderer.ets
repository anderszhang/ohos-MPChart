/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import MyCandleDataSet from '../data/MyCandleDataSet';
import Paint, { LinePaint, RectPaint, Style ,TextPaint} from '../data/Paint';
import ChartAnimator from '../animation/ChartAnimator'
import CandleStickChart from '../charts/CandleStickChart';
import MyCandleData from '../data/MyCandleData';
import MyCandleEntry from '../data/MyCandleEntry';
import ViewPortHandler from '../utils/ViewPortHandler'
import ColorTemplate from '../utils/ColorTemplate';
import type ICandleDataSet from '../interfaces/datasets/ICandleDataSet'
import DataRenderer from './DataRenderer'
import Highlight from '../highlight/Highlight'

export default class MyCandleStickChartRenderer extends DataRenderer {
  protected mRenderPaint: Paint = new Paint();
  protected mPhaseY: number = 1;
  private mBarSpace: number = 0.1;
  private mCandleWidth: number = 6;
  private mContentWidth: number = 200;
  private mMaxSizePercent: number= 0.95;
  protected mChart: CandleStickChart.Model;
  protected mData: MyCandleData;
  private mShadowBuffers: number[] = [];
  private mBodyBuffers: number[] = [];
  private mRangeBuffers: number[] = [];
  private mOpenBuffers: number[] = [];
  private mCloseBuffers: number[] = [];
  constructor(data: MyCandleData, animator: ChartAnimator,
              viewPortHandler: ViewPortHandler, chart: CandleStickChart.Model) {
    super(animator, viewPortHandler);
    this.mData = data;
    this.mContentWidth = viewPortHandler.contentWidth() * this.mMaxSizePercent;
    this.mChart = chart
  }

  public setCandleWidth(val: number): void {
    this.mCandleWidth = val;
  }

  public getCandleWidth(): number {
    return this.mCandleWidth;
  }

  public drawData(): Paint[] {
    let paints: Paint[] = [];
    let candleData: MyCandleData = this.mData;
    let dataSetCount = candleData.getDataSetCount()
    for (let i = 0;i < dataSetCount; i++) {
      let tmpPaints: Array<Paint> = this.drawDataSet(candleData.getDataSetByIndex(i));
      for (let i: number = 0; i < tmpPaints.length; i++) {
        paints.push(tmpPaints[i]);
      }
    }
    return paints;
  }

  protected drawDataSet(dataSet: ICandleDataSet): Paint[] {

    let paints: Paint[] = [];
    let phaseY: number = this.mAnimator.getPhaseY();
    let barSpace: number = this.mBarSpace;
    this.mCandleWidth = this.mCandleWidth * this.mChart.scaleX
    // draw the body
    for (let i = 0;i < dataSet.getEntryCount(); i++) {
      let e = dataSet.getEntryForIndex(i) as MyCandleEntry
      let offset = this.mChart.mHeight + this.mChart.minOffset
      let xPos: number = (e.getX() * this.mChart.scaleX + this.mChart.moveX) - this.mChart.currentXSpace
      let open: number = ((e.getOpen() + offset * (1 - phaseY)) * this.mChart.scaleY + this.mChart.moveY) - this.mChart.currentYSpace
      let close: number = ((e.getClose() + offset * (1 - phaseY)) * this.mChart.scaleY + this.mChart.moveY) - this.mChart.currentYSpace
      let high: number = ((e.getHigh() + offset * (1 - phaseY)) * this.mChart.scaleY + this.mChart.moveY) - this.mChart.currentYSpace
      let low: number = ((e.getLow() + offset * (1 - phaseY)) * this.mChart.scaleY + this.mChart.moveY) - this.mChart.currentYSpace


      //       calculate the shadow
      this.mShadowBuffers[0] = xPos;
      this.mShadowBuffers[2] = xPos;
      this.mShadowBuffers[4] = xPos;
      this.mShadowBuffers[6] = xPos;

      if (open > close) {
        this.mShadowBuffers[1] = high;
        this.mShadowBuffers[3] = open;
        this.mShadowBuffers[5] = low;
        this.mShadowBuffers[7] = close;
      } else if (open < close) {
        this.mShadowBuffers[1] = high;
        this.mShadowBuffers[3] = close;
        this.mShadowBuffers[5] = low;
        this.mShadowBuffers[7] = open;
      } else {
        this.mShadowBuffers[1] = high;
        this.mShadowBuffers[3] = open;
        this.mShadowBuffers[5] = low;
        this.mShadowBuffers[7] = this.mShadowBuffers[3];
      }

      if (xPos + this.mCandleWidth - barSpace > this.mContentWidth) {
        continue;
      }
      let mShadowColor:number=dataSet.getShadowColor();
      this.mRenderPaint.setStroke(mShadowColor)
      // 1. 绘制线条
      let tmpPaint: Array<Paint> = this.drawLines(this.mShadowBuffers, this.mRenderPaint);
      for (let i: number = 0; i < tmpPaint.length; i++) {
        paints.push(tmpPaint[i]);
      }

      // 2. calculate the body
      this.mBodyBuffers[0] = xPos - this.mCandleWidth + barSpace;
      this.mBodyBuffers[1] = close;
      this.mBodyBuffers[2] = (xPos + this.mCandleWidth - barSpace);
      this.mBodyBuffers[3] = open;

      // draw body differently for increasing and decreasing entry
      if (open > close) { // decreasing
        // 设置填充属性
        this.mRenderPaint.setStyle(dataSet.getDecreasingPaintStyle());
        if (dataSet.getDecreasingColor() != ColorTemplate.COLOR_SKIP) {
          this.mRenderPaint.setFill(dataSet.getDecreasingColor());
        } else {
          this.mRenderPaint.setFill(Color.Red);
        }
        this.mRenderPaint.setX(xPos);

        paints.push(this.drawRect(
          this.mBodyBuffers[0], this.mBodyBuffers[1],
          this.mBodyBuffers[2], this.mBodyBuffers[3],
          this.mRenderPaint));

      } else if (open < close) {

        // 设置填充属性
        this.mRenderPaint.setStyle(dataSet.getIncreasingPaintStyle());
        this.mRenderPaint.setStroke(Color.Green);
        this.mRenderPaint.setX(xPos);

        paints.push(this.drawRect(
          this.mBodyBuffers[0], this.mBodyBuffers[1],
          this.mBodyBuffers[2], this.mBodyBuffers[3],
          this.mRenderPaint));
      } else { // equal values

        paints.push(this.drawLine(
          this.mBodyBuffers[0], this.mBodyBuffers[1],
          this.mBodyBuffers[2], this.mBodyBuffers[3],
          this.mRenderPaint));
      }
    }

    return paints;
  }

  // 同步替换
  public drawLines(shadowBuffers: number[], paint: Paint): Paint[] {
    let paints: Paint[] = [];

    let tmp: Array<number> = shadowBuffers;
    for (let i: number = 0; i < tmp.length; i += 4) {
      paints.push(this.drawLine(tmp[i+0], tmp[i+1],
        tmp[i+2], tmp[i+3], paint));
    }
    return paints;
  }

  public drawRect(l: number, t: number, r: number, b: number, paint: Paint): Paint {

    let height: number = (b - t) > 0 ? (b - t) : (t - b);
    let width: number = (r - l) > 0 ? (r - l) : (l - r);
    let rectPaint: RectPaint = new RectPaint();

    rectPaint.setX(paint.getX() - width / 2);
    rectPaint.setY(t < b ? t : b);
    rectPaint.setStyle(paint.getStyle());
    rectPaint.setColor(paint.getColor());
    rectPaint.setFill(paint.fill);
    rectPaint.setWidth(width);
    rectPaint.setHeight(height);

    return rectPaint;
  }

  public drawLine(x1: number, y1: number, x2: number, y2: number, paint: Paint): Paint {
    let linePaint: LinePaint = new LinePaint();
    linePaint.setStartPoint([x1, y1])
    linePaint.setEndPoint([x2, y2])
    linePaint.setStroke(paint.stroke)
    return linePaint;
  }

  public drawExtras(): Paint[] {
    return null;
  }

  public drawHighlighted(indices: Highlight[]): Paint[] {
    return null;
  }

  public drawValues(): Paint[]{
    return null
  }

  public initBuffers(): void {
    // TODO Auto-generated method stub
  }
}